# Matrix
Based on video posted on The Coding Train and Emily Xie's Matrix digital rain.

![alt tag](matrix.gif)

To make simply install the sfml lib on your current OS.
# Mac
```
brew install sfml
```

# Linux
```
apt-get install libsfml-dev
```

Then run make after downloading.